/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.pruebadbms;
import java.lang.String;
import java.util.Vector;

/**
 *
 * @author mferrera
 */


public class Tabla {
    
    private static int nextid = 1;
    public int id = 1;
    int creator;
    String nameTabla;
    int tipoTabla;
    Tabla_Columns[] data;
    String createDDL;
    String modifyDDL;
    String cmmt;
    
    
    

    public Tabla(String tableName, int tableType, int idCreator, String com) {
            nameTabla = tableName;
            id = nextid;
            nextid++;
            creator = idCreator;
            cmmt = com;
            /*if(tableType == 1){
                tipoTabla = tableType;
                data[contArray][0] = id;
                data[contArray][1] = tableName;
                data[contArray][2] = idCreator;
                data[contArray][3] = tableType;
            
                contArray = contArray++;
           
            } else if(tableType == 2){
                tipoTabla = tableType;
                Vistas nuevaView = new Vistas(tableName);
                contArray = contArray++;
            }*/
        
    }
    
    void checkTableType(){
        if(tipoTabla == 1){
            //es tipo base, osea una tabla tabla
        } else if(tipoTabla == 2){
            
        }
    }
    
    int getTablaId(){
        return id;
    }
    
    int getTableCreatorId(){
        return creator;
    }
    
    public String getTableName(){
        return nameTabla;
    }
    
    int getTipoTabla(){
        return tipoTabla;
    }
    
    public String getComment(){
        return cmmt;
    }
    
    public void addColumnas(Tabla_Columns [] objs, int cant){
        data = new Tabla_Columns[cant];
        Tabla_Columns temp;
        String nom, datat;
        boolean primk;
        String ddl = "CREATE TABLE " +nameTabla +" " + "(" ;
        String ddlcmt;
        
        for(int a = 0; a<cant; a++){
            data[a] = objs[a];
            nom = data[a].getNameColumna();
            datat = data[a].getDataType().toString();
            primk = data[a].isPrimaryKey();
  
            if(!primk){
                if(datat.toUpperCase().equals("CLASS JAVA.LANG.INTEGER")){
                    if(a == cant-1){
                    ddl = ddl.concat("  " +nom + "     " + "INTEGER");
                    } else{
                    ddl = ddl.concat(nom + "  " +"INTEGER" + "," + "  ");
                    }
                } else if(datat.toUpperCase().equals("CLASS JAVA.LANG.STRING")){
                    if(a == cant-1){
                    ddl = ddl.concat("  " + nom + "     " +"VARCHAR(100)");
                    } else{
                    ddl = ddl.concat(nom + "   " + "VARCHAR(100)" + "," + "  ");
                    }
                }
            } else{
                if(datat.toUpperCase().equals("CLASS JAVA.LANG.INTEGER")){
                    if(a == cant-1){
                    ddl = ddl.concat("  " +nom + "     " + "INTEGER PRIMARY KEY" );
                    } else{
                    ddl = ddl.concat(nom + "   " + "INTEGER PRIMARY KEY" +  "," + "  ");
                    }
                } else if(datat.toUpperCase().equals("CLASS JAVA.LANG.STRING")){
                    if(a == cant-1){
                    ddl = ddl.concat("  " + nom + "     " + "VARCHAR(100) PRIMARY KEY" );
                    } else{
                    ddl = ddl.concat(nom + "   " + "VARCHAR(100) PRIMARY KEY" + "," + "  ");
                    }
                }
            }
        }
        ddl = ddl.concat(");");
        ddl = ddl.concat(" " + "COMMENT ON TABLE" + " "+ nameTabla + " " + "IS" + '"' + cmmt + '"' + ";");
        createDDL = ddl;
        
    }

        
    public String getCreateDDL(){
       return createDDL;
    }
    
        
    
}


