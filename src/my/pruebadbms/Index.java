/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.pruebadbms;

/**
 *
 * @author mferrera
 */
public class Index {
    private static int nextid = 1;

    public int id = 1;
    public int tableID;
    String indexName;
    int indexType;
    
    
    public Index(int tabID, String nom, int type){
        id = nextid;
        nextid++;
        tableID = tabID;
        indexName = nom + "_IDX";
        indexType = type;
        
        //1 para primary key, 2 para foreign key
    }
    
    public int getIndexId(){
        return id;
    }
    
    public String getIndexName(){
        return indexName;
    }
    
    public int getIndexTabId(){
        return tableID;
    }
    
    public int getIndexType(){
        return indexType;
    }
    
}
