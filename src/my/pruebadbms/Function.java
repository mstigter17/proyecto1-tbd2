/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.pruebadbms;

/**
 *
 * @author mferrera
 */
public class Function {
    
    private static int nextid = 1;
    private int id = 1;
    String funcName;
    String valorDeRetorno;
    String nombreValorDeRetorno;
    
    User userCreador;
    String currConnName;
    
    String createDDL;
    
    public Function(String nom, String vDR, String nomVDR, User uc){
        id = nextid;
        nextid++;
        funcName = nom;
        valorDeRetorno = vDR;
        nombreValorDeRetorno = nomVDR;
        userCreador = uc;
    }
    
    int getFuncId(){
        return id;
    }
    
    String getFuncName(){
        return funcName;
    }
    
    String getValorDeRetorno(){
        return valorDeRetorno;
    }
    
    String getValorDeRetornoNombre(){
        return nombreValorDeRetorno;
    }
    
    int getUserCreadorId(){
        return userCreador.getUserId();
    }
    
    public void setCurrConnName(String s)
    {
        currConnName = s;
    }    
    public String getCreateDDL(){
        createDDL = "CREATE FUNCTION " + '"' + currConnName + '"' + "." + '"' + funcName + "_FUNC" + '"' + " ( ** IN PARAMETER_NAME PARAMETER_TYPE ** ) RETURNS " + valorDeRetorno + " DETERMINISTIC BEGIN DECLARE " + '"' + nombreValorDeRetorno + '"' + " " + valorDeRetorno + "; [ ** TYPE THE FUNCTION STATEMENT HERE ** ] RETURN " + '"' + nombreValorDeRetorno + '"' + "; END";
        
        return createDDL;
    }
    
}
