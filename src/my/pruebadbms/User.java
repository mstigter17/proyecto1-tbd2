/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.pruebadbms;
import java.lang.String;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author mferrera
 */
public class User {
    private static int nextid = 1;
    public int id = 1;
    String username;
    String password;
    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    Date fechaCreacion;
    String createDDL;
    
    
    public User(String user, String pass){
            id = nextid;
            nextid++;
            username = user;
            password = pass;
            fechaCreacion = new Date();
            
        
    }
    public int getUserId(){
        return id;
    }
    
    public String getUserName(){
        return username;
    }
    
    public String getPassword(){
        return password;
    }
    
    public String getFechaCreacion(){
        return dateFormat.format(fechaCreacion);
    }
    
    public String getCreateDDL(){
        createDDL = "CREATE USER " + username + " IDENTIFIED BY " + password + ";";
        return createDDL;
    }
     
    
     
    
}
