/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.pruebadbms;
import java.lang.String;
/**
 *
 * @author mferrera
 */
public class Triggers {
    
    private static int nextId = 1;
    public int id = 1;
    String triggerName;
    String triggerTime;
    String accion;
    String tablaDeTrigger;
    User userCreador;
    String currConnName;
    
    
    String createDDL;
    
    public Triggers(String triggName, String time, String a, String tb, User uc){
        id = nextId;
        nextId++;
        
        triggerName = triggName;
        triggerTime = time;
        accion = a;
        tablaDeTrigger = tb;
        userCreador = uc;

    }
    
     int getViewId(){
        return id;
    }
    
    String getTriggName(){
        return triggerName;
    }
    
    String getTriggerTime(){
        return triggerTime;
    }
    
    String getTriggerAccion(){
        return accion;
    }
    
    String getTablaNameDeTrigg(){
        return tablaDeTrigger;
    }
    
    public void setConexion(String connu){
        currConnName = connu;
    }
    

    int getUserCreadorId(){
        return userCreador.getUserId();
    }
    
    public String getCreateDDL(){
        createDDL = "CREATE TRIGGER " + '"' + triggerName + '"' + " " + triggerTime + " " + accion + " ORDER 1 ON " + '"' + currConnName + '"' + "." + '"' + tablaDeTrigger + '"' + "FOR EACH ROW BEGIN [ ** TYPE THE TRIGGER STATEMENT HERE **] END;";
        return createDDL;
    }
    
    
    
}
