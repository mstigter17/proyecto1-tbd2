/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.pruebadbms;

/**
 *
 * @author mferrera
 */
public class Conexion {
    private static int nextid = 1;

    public int id = 1;
    int currentUser;
    User currU;
    String connectionName;
    String userCreador;
    boolean active;
    
    String createDDL;
    String grantDDL;
    
    String[] columnNamesTab = {"Tabla ID", "Nombre de Tabla","Creador de la Tabla","Tipo de Tabla"};
    Object [][] dataTab;
     int contArrayTab;
     
      String[] columnNamesIdx = {"Index ID", "Nombre de Index","Tipo de Index","Tabla ID"};
    Object [][] dataIdx;
     int contArrayIdx;
    
    
    
    public Conexion(User u, String n){
        id = nextid;
        nextid++;
        userCreador = u.getUserName();
        connectionName = n;

        currU = u;

        currentUser = u.getUserId();
        active = true;
        
        dataTab = new Object[200][4];
        contArrayTab = 1;
        
        dataIdx = new Object[200][4];
        contArrayIdx = 1;
    }
    
    public int getConnId(){
        return id;
    }
    
    public String getConnName(){
        return connectionName;
    }
    
    public String getConnCreator(){
        return userCreador;
    }
    
    public int getCurrentUser(){
        return currentUser;
    }
    
    public boolean isActive(){
        return active;
    }
    
    public void setStatus(boolean c){
        active = c;
    }


    
    public String getGrantDDL(){
        grantDDL = "GRANT CONNECT TO " + currU.getUserName() +";";
        return grantDDL;
    }
    
    public void addTableToTbTable(Tabla t){
        dataTab[contArrayTab][0] = t.getTablaId();
        dataTab[contArrayTab][1] = t.getTableName().toUpperCase();
        dataTab[contArrayTab][2] = t.getTableCreatorId();
        dataTab[contArrayTab][3] = t.getTipoTabla();
        
        contArrayTab = contArrayTab+1;
    }
    
    public void addIndexToIdxTable(Index i){
        dataIdx[contArrayIdx][0] = i.getIndexId();
        dataIdx[contArrayIdx][1] = i.getIndexName().toUpperCase();
        dataIdx[contArrayIdx][2] = i.getIndexType();
        dataIdx[contArrayIdx][3] = i.getIndexTabId();
        
        contArrayIdx = contArrayIdx+1;
    }
    
    public Object checkifTabExists(String t){
         for(int i = 1; i < contArrayTab; i++){
             if(dataTab[i][1].equals(t)){
                  return dataTab[i][1];
             }
         }
         return null;
     }
    
    public Object getTableId(String t){
         for(int i = 1; i < contArrayTab; i++){
             if(dataTab[i][1].equals(t)){
                  return dataTab[i][0];
             }
         }
         return null;
     }
    
    public Object getTableCreatorId(String t){
         for(int i = 1; i < contArrayTab; i++){
             if(dataTab[i][1].equals(t)){
                  return dataTab[i][2];
             }
         }
         return null;
     }
    
    public Object getTableType(String t){
         for(int i = 1; i < contArrayTab; i++){
             if(dataTab[i][1].equals(t)){
                  return dataTab[i][3];
             }
         }
         return null;
     }
    
    
    
    public void showTables(){
        for(int i = 1; i <contArrayTab; i++){
             System.out.println("Table ID:"+dataTab[i][0]);
             System.out.println("Table Name:"+dataTab[i][1].toString());
             System.out.println("Table Creator: "+ dataTab[i][2].toString());
             System.out.println("Table Type: "+ dataTab[i][3].toString());
         }
    }
    
    
    

     
    
    
    
    
    
}
