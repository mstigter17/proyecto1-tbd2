/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.pruebadbms;

/**
 *
 * @author mferrera
 */
public class Tabla_Columns {
    
    boolean primaryKey;
    String nameColumna;
    Class dataType;
  
    public Tabla_Columns(boolean pk, String name, Class dT){
        primaryKey = pk;
        nameColumna = name;
        dataType = dT;
    }
    
    public String getNameColumna(){
        return nameColumna;
    }
    
    public boolean isPrimaryKey(){
        return primaryKey;
    }
    
    public Class getDataType(){
        return dataType;
    }
    
    public void agregarDatos(){
        
    }
    
    
    
}
