/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.pruebadbms;
import java.lang.String;
/**
 *
 * @author mferrera
 */
public class Vistas {
    private static int nextid = 1;
    private int id = 1;
    String viewName;
    User userCreador;
    String createDDL;
    
    
    public Vistas(String nom, User uc){
        id = nextid;
        nextid++;
        viewName = nom;
        userCreador = uc;
    }
    
    int getViewId(){
        return id;
    }
    
    String getViewName(){
        return viewName;
    }
    
    int getUserCreadorId(){
        return userCreador.getUserId();
    }
    
    public String getCreateDDL(){
        createDDL = "CREATE VIEW " + '"' + "SYSTEM" + '"' + "." + '"' + viewName + '"' + "AS  [ TYPE THE SELECT STATEMENT HERE ] ";
        return createDDL;
    }
    
}
