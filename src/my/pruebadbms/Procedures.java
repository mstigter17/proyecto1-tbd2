/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package my.pruebadbms;

/**
 *
 * @author mferrera
 */
public class Procedures {
    private static int nextId = 1;
    public int id = 1;
    String procName;
    User userCreador;
    int uCreadorId;
    String currConnName;
    String createDDL;
    
    public Procedures(String pName, User uCre){
        id = nextId;
        nextId++;
        procName = pName;
        uCreadorId = uCre.getUserId();
        
    }
    
    public int getProcID(){
        return id;
    }
    
    public String getProcName(){
        return procName;
    }
    
    public int getUserCreadorId(){
        return uCreadorId;
    }
    
    public User getUserCreador(){
        return userCreador;
    }
    
    public void setCurrConn(String s){
        currConnName = s;
    }
    
    public String getCreateDDL(){
        createDDL = "CREATE PROCEDURE " + '"' + currConnName + '"' + "." + '"' + procName +  "_PROC"+ '"' + "([IN/OUT/IN-OUT] param_name param_type) RESULT( column_name column_type ) BEGIN Type the procedure statements here END;";

        
        return createDDL;
    }
    
    
    
}
